<?php

namespace Tests\Unit;

use App\Models\Item;
use Illuminate\Support\Str;
use PHPUnit\Framework\TestCase;

class ItemTest extends TestCase
{
    private $item;

    protected function setUp(): void
    {
        parent::setUp();

        $this->item = new Item([
            'name' => 'Blabla1',
            'content' => 'hahahahahahahahahahahahaha'
        ]);
    }

    public function testIsValidName()
    {
        $this->assertTrue($this->item->isValid());
    }

    public function testNameNull()
    {
        $this->item->name = null;
        $this->assertFalse($this->item->isValid());
    }

    public function testNameEmpty()
    {
        $this->item->name = '';
        $this->assertFalse($this->item->isValid());
    }

    public function testLongContent()
    {
        $this->item->content = Str::random(1700);
        $this->assertFalse($this->item->isValid());
    }
}

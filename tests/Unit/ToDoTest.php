<?php

namespace Tests\Unit;

use App\Models\Item;
use App\Models\TodoList;
use Tests\TestCase;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Str;

class TodoListTest extends TestCase
{
    private $item;
    private $user;
    private $todoList;

    protected function setUp(): void
    {
        parent::setUp();

        $this->item = new Item([
            'name' => 'Blabla1',
            'content' => 'hahahahahahahahahahahahaha',
            'created_at' => Carbon::now()->subHour()
        ]);

        $this->user = new User([
            'firstname' => 'Randy',
            'lastname' => 'Mb',
            'email' => 'tests@tests.com',
            'password' => 'im_the_password',
            'birthday' => Carbon::now()->subDecades(21)->toDateString()
        ]);

        $this->todoList = $this->getMockBuilder(TodoList::class)
            ->onlyMethods(['ItemsCount', 'getLastItem'])
            ->getMock();
        $this->todoList->user = $this->user;
    }

    public function testAddItem()
    {
        $this->todoList->expects($this->once())->method('ItemsCount')->willReturn(1);
        $this->todoList->expects($this->any())->method('getLastItem')->willReturn($this->item);

        $canAddItem = $this->todoList->canAddItem($this->item);
        $this->assertNotNull($canAddItem);
        $this->assertEquals('La liste', $canAddItem->name);
    }

    public function testAddItemFull()
    {
        $this->todoList->expects($this->any())->method('ItemsCount')->willReturn(10);

        $this->expectException('Exception');
        $this->expectExceptionMessage('Todo list pleine');

        $this->todoList->canAddItem($this->item);
    }

    public function testAddItemTooRecent()
    {
        $this->todoList->expects($this->any())->method('ItemsCount')->willReturn(0);

        $recentItem = $this->item->replicate();
        $recentItem->created_at = Carbon::now()->subMinutes(45);
        $this->todoList->expects($this->any())->method('getLastItem')->willReturn($recentItem);

        $this->expectException('Exception');
        $this->expectExceptionMessage('Last item is too recent');

        $this->todoList->canAddItem($this->item);
    }

    public function testAddItemUserNotValid()
    {
        $this->todoList->user->email = 'test';

        $this->todoList->expects($this->any())->method('ItemsCount')->willReturn(0);
        $this->todoList->expects($this->any())->method('getLastItem')->willReturn($this->item);

        $this->expectException('Exception');
        $this->expectExceptionMessage('User null - invalid');

        $this->todoList->canAddItem($this->item);
    }

    public function testAddItemIsNull()
    {
        $this->expectException('Exception');
        $this->expectExceptionMessage('Item null - invalid');

        $badItem = $this->item->replicate();
        $badItem->name = '';

        $this->todoList->canAddItem($badItem);
    }

    public function testName()
    {
        $todoList = TodoList::make([
            'name' => 'todo_name',
            'description' => 'here a description'
        ]);

        $this->assertTrue($todoList->isValid());
    }

    public function testNoDescription()
    {
        $todoList = TodoList::make([
            'name' => 'todo_name',
        ]);

        $this->assertTrue($todoList->isValid());
    }

    public function testNoName()
    {
        $todoList = TodoList::make([
            'description' => 'here a description'
        ]);

        $this->assertFalse($todoList->isValid());
    }

    public function testDescriptionTooLong()
    {
        $todoList = TodoList::make([
            'name' => 'todo_name',
            'description' => Str::random(410)
        ]);

        $this->assertFalse($todoList->isValid());
    }
}
